<?php include('header.php'); ?>	
	<header>		
		<style>
		header{
			position:fixed;
			top:0;
			z-index:20;
		}		
		#contentSimulator0{
			display:block;
		}
		</style>
		<?php
		include('bar.php');
		?>	
		<div id='menuSimulator'>
			<ul>
				<li>
					<a href='#' onClick='return anchor("body");' id='menuSim0' class='selected'>Financiamento</a>
				</li>
				<li>
					<a href='#' onClick='return anchor("#rent");' id='menuSim1'>Imóveis com renda</a>
				</li>
			</ul>
		</div>
	</header>
	<section>
		<div id='spacer'></div>
		<div id='titleSimulator'>
			<span></span>
			SIMULADOR DE FINANCIAMENTO
		</div>
		<form id='formSimulator' method='post' action=''>
			<fieldset>
				<span>
					<label>Tipo do Imóvel</label>
					<select size='1' name='type'>						
						<option value='0'>Residencial</option>
						<option value='1'>Loren Ipsum</option>
						<option value='2'>Suspendisse risus</option>
						<option value='3'>Proin sed</option>
					</select>
				</span>
				<span>
					<label>Valor da Renda</label>
					<div>	
						<input type='text' name='value' value='9999,99' maxlength='8'/>
					</div>
				</span>
				<span>
					<label>Entrada</label>
					<div>
						<input type='text' name='enter' value='9999,99' maxlength='8'/>
					</div>
				</span>
				<span>
					<label>Prazo</label>
					<select size='1' name='time'>
						<?php
						for($x=3; $x<=10; $x++){
						?>
							<option value='<?php echo $x; ?>'><?php echo $x; ?> anos</option>
						<?php
						}
						?>						
					</select>
				</span>
				<span>
					<label>Sua Idade</label>
					<select size='1' name='year'>
						<?php
						for($x=18; $x<30; $x++){
						?>
							<option value='<?php echo $x; ?>'><?php echo $x; ?> anos</option>
						<?php
						}
						?>
						<option value='30+'>maior que 30 anos</option>
					</select>
				</span>				
				<input type='submit' name='' value='Calcular'/>				
			</fieldset>
		</form>
		<div class='full'>
			<div id='simulatorLeft'>
				<span></span>
				<p>Tire suas dúvidas</p>
				<ul>
					<li>
						<a href='#' onClick='return viewBox(0);'>Linhas de credito CrediPronto</a>
					</li>
					<li>
						<a href='#' onClick='return viewBox(1);'>Caracteristicas do financiamento</a>
					</li>
					<li>
						<a href='#' onClick='return viewBox(2);'>Solicitando o financiamento</a>
					</li>
					<li>
						<a href='#' onClick='return viewBox(3);'>Usando o FGTS</a>
					</li>
					<li>
						<a href='#' onClick='return viewBox(4);'>Quitando as parcelas</a>
					</li>
					<li>
						<a href='#' onClick='return viewBox(5);'>Perguntas frequentes</a>
					</li>
				</ul>
			</div>
			<div id='simulatorRight'>
				<div id='contentSimulator0' class='contentSimulator'>
					<p>A CrediPronto! oferece linhas de crédito especiais para financiamento imobiliário. Saiba como funcionam:</p>
					<p><b>SISTEMA FINANCEIRO DE HABITAÇÃO (SFH)</b></p>
					<p>No SFH, o cliente conta com taxas de juros menores e pode utilizar os recursos do FGTS.</p>
					<p>
						Para solicitar um financiamento no SFH, é preciso atender às seguintes condições:<br/>
						- O valor de avaliação e de compra e venda do imóvel deve ser igual ou inferior a R$500 mil;<br/>
						- O valor do financiamento deve ser igual ou inferior a R$400 mil;<br/>
						- O imóvel deve ser residencial.<br/>
					</p>
					<p><b>CARTEIRA HIPOTECÁRIA</b></p>
					<p>A Carteira Hipotecária é a linha de crédito utilizada para financiamento de imóveis com valor acima de R$500 mil ou imóveis comerciais de qualquer valor. Ela está disponível para qualquer tipo de imóvel urbano, independentemente dos valores envolvidos no negócio, de acordo com as seguintes condições:</p>
					<p>
						- Valor mínimo de financiamento: R$50 mil;<br/>
						- Prazo máximo de financiamento: 360 meses (30 anos) para imóveis residenciais e 12 anos (144 meses) para imóveis comerciais.
					</p>
				</div>
				<div id='contentSimulator1' class='contentSimulator'>
					<p><b>GARANTIA DO FINANCIAMENTO</b></p>
					<p>A garantia será a alienação fiduciária ou hipoteca, ou seja, a transferência do imóvel em garantia do pagamento da dívida.</p>
					<p><b>REAJUSTE DA PRESTAÇÃO E DO SALDO DEVEDOR</b></p>
					<p>Os reajustes da prestação e do saldo devedor serão mensais, segundo o índice de atualização da Caderneta de Poupança, que atualmente é a TR (Taxa Referencial).</p>
					<p><b>TRANSFERÊNCIA DE FINANCIAMENTO</b></p>
					<p>Caso você esteja interessado em um imóvel cujo proprietário tenha um Financiamento Imobiliário do Banco Itaú, o valor do saldo devedor pode ser transferido à você nas mesmas condições de um novo financiamento.</p>
					<p><b>SEGUROS</b></p>
					<p>Todos os financiamentos da CrediPronto! são acompanhados de dois tipos de seguros, cobrados mensalmente, que aumentam a sua tranqüilidade no caso de algum infortúnio.</p>
					<p>As coberturas previstas para os financiamentos são:</p>
					<p><b>Morte e Invalidez Permanente (MIP);</b></p>
					<p>Caso um dos clientes participantes do financiamento venha a falecer ou ficar incapaz de exercer sua profissão, prejudicando assim sua capacidade de pagar o financiamento, esse seguro quitará a parcela correspondente do saldo devedor, segundo o percentual da renda total pelo qual o cliente é responsável.</p>
					<p>Exemplo: se o saldo devedor de um financiamento composto por duas pessoas for de R$ 100 mil e ocorrer o falecimento do cliente que apresentou 30% do total de rendimentos, o seguro quitará R$ 30 mil do saldo devedor, ficando o outro cliente responsável por continuar pagando normalmente o restante da dívida (R$ 70 mil).</p>
					<p><b>Danos Físicos ao Imóvel (DFI)</b></p>
					<p>Esse seguro cobre eventuais danos que possam ocorrer ao imóvel no período do financiamento, exceto problemas oriundos do período de construção.</p>
					<p>A cobertura DFI incide sobre o valor de avaliação do imóvel e responderá pelas despesas de reparos, inclusive quitando as prestações do financiamento enquanto o imóvel estiver em obras.</p>
					<p><b>Custo de administração</b></p>
					<p>É uma tarifa cobrada mensalmente com a prestação, referente aos serviços de administração do financiamento.</p>
					<p><b>INCORPORAÇÃO DE CUSTOS ADICIONAIS</b></p>
					<p>Toda compra de imóvel exige o pagamento de Imposto de Transferência de Bens Imóveis (ITBI) e o registro do imóvel em nome do cliente. Quando a aquisição é feita com um financiamento, deve-se registrar a garantia em nome do banco.</p>
					<p>O ITBI é cobrado pelas prefeituras das cidades, após a assinatura do contrato, a ser documentado no Cartório de Registro de Imóveis. Para que esse encargo não pese no orçamento do cliente, a CrediPronto! possibilita sua incorporação no seu financiamento que será contratado, juntamente com as despesas de registro do contrato. Para solicitar esse benefício, basta que você informe na proposta o valor total que necessita utilizar.</p>
					<p>O valor indicado por você será reembolsado após a devolução do contrato registrado, por meio de crédito em conta corrente do Banco Itaú.</p>
					<p><b>COMO UTILIZAR O FGTS?</b></p>
					<p>Leia a ajuda Usando o FGTS</p>
					<p><b>CALCULANDO A SUA PRESTAÇÃO</b></p>
					<p>O valor da primeira prestação do financiamento deve ser de, no máximo, 30% da renda líquida (após os descontos, como IR, INSS e outros) dos envolvidos (máximo duas pessoas) no financiamento, uma vez que seja confirmada a capacidade de pagamento da prestação.</p>
					<p>Importante: A CrediPronto! permite que cada financiamento seja formado por, no máximo, duas pessoas, as quais poderão compor rendimentos para atingir a renda mínima exigida (caso um dos clientes seja casado ou convivente marital, somente é permitida a composição de renda com seu respectivo cônjuge).</p>
				</div>
				<div id='contentSimulator2' class='contentSimulator'>
					<p><b>JÁ POSSUO UM FINANCIAMENTO IMOBILIÁRIO, POSSO FAZER OUTRO?</b></p>
					<p>Sim, desde que comprove renda suficiente para assumir ambos os financiamentos, e que o novo financiamento se enquadre na política de crédito vigente.</p>
					<p><b>É POSSÍVEL FAZER TRANSFERÊNCIA DO FINANCIAMENTO BANCÁRIO?</b></p>
					<p>Sim. A transferência do financiamento somente será possível mediante a concessão de um novo financiamento ao solicitante, que terá como base política de crédito e legislação vigentes à época da concessão. O cálculo da renda mínima necessária está condicionado às variáveis da nova operação, tais como: valor do financiamento, idade do proponente e ao valor de avaliação do imóvel.</p>
					<p><b>IMÓVEIS EM CONSTRUÇÃO PODEM SER FINANCIADOS?</b></p>
					<p>Não. Apenas os imóveis que estão concluídos e com o habite-se (autorização dada pela Prefeitura para que se possa ocupar e utilizar um imóvel recém-construído ou reformado)</p>
					<p><b>SE A MINHA RENDA NÃO ATINGIR O MÍNIMO EXIGIDO, POSSO SOMAR A RENDA COM A DE OUTRA PESSOA?</b></p>
					<p>É permitido que cada financiamento seja formado por, no máximo, duas pessoas, que poderão compor a renda exigida. Clientes casados, inclusive por União Estável, somente é permitida a composição de renda com seu respectivo cônjuge.</p>
					<p><b>O QUE É ITBI E COMO ELE É CALCULADO?</b></p>
					<p>Toda compra de imóvel exige o pagamento do Imposto de Transmissão de Bens Imóveis e o registro do imóvel. Quando a aquisição é feita com um financiamento, deve-se registrar a garantia em nome do Agente Financeiro. O ITBI é cobrado pela Prefeitura local.</p>
					<p><b>POSSO INCLUIR O ITBI NO VALOR DO FINANCIAMENTO?</b></p>
					<p>A CrediPronto! possibilita que você incorpore esses valores no financiamento (ITBI e registro do contrato, limitados a 5% do valor do financiamento), diluindo-os nas prestações. Para que você utilize esse benefício, a CrediPronto! irá orientá-lo e indicará o valor total que você necessita utilizar no campo específico da Proposta de Financiamento.</p>
					<p><b>É OBRIGATÓRIA A ABERTURA DE CONTA CORRENTE NO ITAÚ?</b></p>
					<p>Somente para o cliente comprador, pois o financiamento imobiliário da CrediPronto! é operacionalizado por meio de débito automático. Por esta razão, é necessário que possua uma conta corrente no Itaú.</p>
					<p><b>O QUE É CONTRATO DE COMPROMISSO DE COMPRA E VENDA?</b></p>
					<p>É a formalização do compromisso comercial entre o comprador e o vendedor do imóvel. Neste contrato o vendedor é chamado de promitente-vendedor e o comprador de compromissário-comprador.</p>
					<p><b>PODE SER FEITA VENDA DE IMÓVEL POR PROCURAÇÃO?</b></p>
					<p>Sim. Essa procuração deve ser pública.</p>
					<p><b>POR QUE DEVO FAZER O REGISTRO DO IMÓVEL?</b></p>
					<p>O registro do imóvel declara quem é o verdadeiro dono do imóvel. De acordo com o Código Civil Brasileiro, a propriedade (imóvel) somente é transferida após o registro da escritura no Cartório de Registro de Imóveis. Portanto, não vale assinar somente a escritura ou o contrato de venda e compra do imóvel. Deve-se realizar o registro, pois quem não registra não é considerado de fato o proprietário.</p>
					<p>O registro deve ser realizado no Cartório de Registro de Imóveis da sua localização (bairro, município ou comarca).</p>
					<p><b>QUAIS AS PROVIDÊNCIAS QUE DEVERÃO SER TOMADAS AO QUITAR O IMÓVEL?</b></p>
					<p>Ao quitar todas as prestações do financiamento, providencie a Escritura Definitiva no Tabelionato de Notas. Leve todos os documentos pessoais (dos compradores), respectiva prova de quitação e o contrato. Em seguida registre a Escritura no Cartório de Registro de Imóveis competente para a efetiva garantia da propriedade do imóvel. Após, peça por escrito na Prefeitura a alteração do imposto territorial para seu nome e seu endereço.</p>
				</div>
				<div id='contentSimulator3' class='contentSimulator'>
					<p><b>POSSO ADQUIRIR O IMÓVEL EM QUALQUER LUGAR, USANDO O FGTS?</b></p>
					<p>Não. Há restrições. O imóvel deve estar situado no município onde o comprador exerce sua ocupação principal, ou em município limítrofe, ou pertencente à mesma região metropolitana. Poderá ser escolhido imóvel no município onde o cliente mora, desde que comprove que não possua nenhum imóvel em todo o território nacional.</p>
					<p><b>QUAL O VALOR MÁXIMO DO IMÓVEL QUE EU POSSO FINANCIAR COM O FGTS?</b></p>
					<p>O valor do imóvel residencial a ser adquirido pelo FGTS não pode ultrapassar R$ 500 mil, sendo financiado até R$ 450 mil.</p>
					<p>Consulte o saldo de seu FGTS</p>
					<p><b>O FINANCIAMENTO É LIBERADO NA HORA?</b></p>
					<p>A possibilidade de utilização dos recursos do FGTS somente será analisada após a aprovação do crédito do cliente, quando serão solicitados os documentos necessários para a formalização do pedido à Caixa Econômica Federal.</p>
					<p>Assim, caso seja verificado que os recursos do FGTS estão indisponíveis ou que os valores encontrados não são os mesmos informados na Proposta de Financiamento, o processo retornará à análise de crédito.</p>
					<p><b>POSSO USAR O FGTS PARA ADQUIRIR IMÓVEL PARA O MEU FILHO?</b></p>
					<p>Não. É vedada a sua utilização para moradia exclusiva de familiares, dependentes ou terceiros.</p>
					<p><b>EXISTE ENQUADRAMENTO PARA O USO DO FGTS PARA O IMÓVEL?</b></p>
					<p>Sim. O imóvel além de estar em condições para moradia, não poderá ter sido adquirido pelo(s) vendedor(es) com recursos do FGTS, nos últimos três anos.</p>
					<p><b>COMPREI UM IMÓVEL NA PLANTA, PRETENDO QUITAR NA ENTREGA DAS CHAVES, POSSO UTILIZAR O FGTS MESMO TENDO OUTRO IMÓVEL?</b></p>
					<p>Não. Se você já possui outro imóvel na localidade em que reside ou trabalha, não poderá utilizar o FGTS.</p>
					<p><b>MORO EM SÃO PAULO, ONDE JÁ POSSUO 2 IMÓVEIS EM MEU NOME. MAS TRABALHO HÁ MAIS DE 3 ANOS EM ITAPECERICA DA SERRA, POSSO USAR MEU FGTS PARA COMPRAR UM APARTAMENTO EM ITAPECERICA DA SERRA?</b></p>
					<p>Não, pois Itapecirica da Serra faz parte da região metropolitana de São Paulo além de ser município limítrofe de São Paulo. O proponente não pode possuir imóvel no munícipio que reside ou exerce sua ocupação principal, município limítrofe ou pertencente à região metropolitana.</p>
					<p><b>O QUE É MUNICÍPIO LIMÍTROFE?</b></p>
					<p>Entende-se por município limítrofe as regiões vizinhas. Exemplo: São Paulo x Guarulhos são regiões consideradas limítrofes (vizinhas), então se o cliente mora em Guarulhos e trabalha em São Paulo e quer comprar o imóvel em São Paulo poderá usar o FGTS, se não possuir imóvel. São Paulo x Praia Grande/SP não são regiões limítrofes (vizinhas), e neste caso se o cliente trabalha e mora em São Paulo e quer comprar o imóvel na Praia Grande, não poderá usar o FGTS.</p>
					<p><b>POSSO UTILIZAR O FGTS PARA AMORTIZAR O SALDO DEVEDOR EM UM IMÓVEL QUE FOI AVALIADO COM VALOR SUPERIOR AO PERMITIDO DE R$ 500.000,00?</b></p>
					<p>Não. O limite aceito pelas regras de uso do FGTS é que o imóvel tenha o valor de no máximo R$ 500.000,00.</p>
					<p><b>POSSO UTILIZAR O FGTS PARA AMORTIZAR O FINANCIAMENTO IMOBILIÁRIO?</b></p>
					<p>Sim, desde que o financiamento se enquadre nas regras da CEF você pode utilizar para amortização extraordinária do saldo devedor para redução do valor da prestação ou amortização extraordinária do saldo devedor para redução do prazo contratual.</p>
					<p><b>FINANCIEI UM IMÓVEL NO VALOR DE R$ 600.000,00 E GOSTARIA DE UTILIZAR O FGTS PARA AMORTIZAR O SALDO DEVEDOR. POSSO?</b></p>
					<p>Não. Para utilização do FGTS, o limite máximo do valor do imóvel é de R$ 500.000,00.</p>
					<p><b>ESTOU COM PRESTAÇÕES DO MEU FINANCIAMENTO IMOBILIÁRIO EM ATRASO. GOSTARIA DE UTILIZAR O FGTS PARA FICAR EM DIA E REDUZIR PARTE DO SALDO DEVEDOR. ISSO É POSSÍVEL?</b></p>
					<p>Não. Para qualquer utilização do FGTS, tanto para abatimento nas prestações quanto para amortização ou liquidação, as prestações do financiamento deverão estar rigorosamente em dia.</p>
				</div>
				<div id='contentSimulator4' class='contentSimulator'>
					<p><b>GARANTIA DO FINANCIAMENTO</b></p>
					<p>A garantia será a alienação fiduciária ou hipoteca, ou seja, a transferência do imóvel em garantia do pagamento da dívida.</p>
					<p><b>REAJUSTE DA PRESTAÇÃO E DO SALDO DEVEDOR</b></p>
					<p>Os reajustes da prestação e do saldo devedor serão mensais, segundo o índice de atualização da Caderneta de Poupança, que atualmente é a TR (Taxa Referencial).</p>
					<p><b>TRANSFERÊNCIA DE FINANCIAMENTO</b></p>
					<p>Caso você esteja interessado em um imóvel cujo proprietário tenha um Financiamento Imobiliário do Banco Itaú, o valor do saldo devedor pode ser transferido à você nas mesmas condições de um novo financiamento.</p>
					<p><b>SEGUROS</b></p>
					<p>Todos os financiamentos da CrediPronto! são acompanhados de dois tipos de seguros, cobrados mensalmente, que aumentam a sua tranqüilidade no caso de algum infortúnio.</p>
					<p>As coberturas previstas para os financiamentos são:</p>
					<p><b>Morte e Invalidez Permanente (MIP);</b></p>
					<p>Caso um dos clientes participantes do financiamento venha a falecer ou ficar incapaz de exercer sua profissão, prejudicando assim sua capacidade de pagar o financiamento, esse seguro quitará a parcela correspondente do saldo devedor, segundo o percentual da renda total pelo qual o cliente é responsável.</p>
					<p>Exemplo: se o saldo devedor de um financiamento composto por duas pessoas for de R$ 100 mil e ocorrer o falecimento do cliente que apresentou 30% do total de rendimentos, o seguro quitará R$ 30 mil do saldo devedor, ficando o outro cliente responsável por continuar pagando normalmente o restante da dívida (R$ 70 mil).</p>
					<p><b>Danos Físicos ao Imóvel (DFI)</b></p>
					<p>Esse seguro cobre eventuais danos que possam ocorrer ao imóvel no período do financiamento, exceto problemas oriundos do período de construção.</p>
					<p>A cobertura DFI incide sobre o valor de avaliação do imóvel e responderá pelas despesas de reparos, inclusive quitando as prestações do financiamento enquanto o imóvel estiver em obras.</p>
					<p><b>Custo de administração</b></p>
					<p>É uma tarifa cobrada mensalmente com a prestação, referente aos serviços de administração do financiamento.</p>
					<p><b>INCORPORAÇÃO DE CUSTOS ADICIONAIS</b></p>
					<p>Toda compra de imóvel exige o pagamento de Imposto de Transferência de Bens Imóveis (ITBI) e o registro do imóvel em nome do cliente. Quando a aquisição é feita com um financiamento, deve-se registrar a garantia em nome do banco.</p>
					<p>O ITBI é cobrado pelas prefeituras das cidades, após a assinatura do contrato, a ser documentado no Cartório de Registro de Imóveis. Para que esse encargo não pese no orçamento do cliente, a CrediPronto! possibilita sua incorporação no seu financiamento que será contratado, juntamente com as despesas de registro do contrato. Para solicitar esse benefício, basta que você informe na proposta o valor total que necessita utilizar.</p>
					<p>O valor indicado por você será reembolsado após a devolução do contrato registrado, por meio de crédito em conta corrente do Banco Itaú.</p>
					<p><b>COMO UTILIZAR O FGTS?</b></p>
					<p>Leia a ajuda Usando o FGTS</p>
					<p><b>CALCULANDO A SUA PRESTAÇÃO</b></p>
					<p>O valor da primeira prestação do financiamento deve ser de, no máximo, 30% da renda líquida (após os descontos, como IR, INSS e outros) dos envolvidos (máximo duas pessoas) no financiamento, uma vez que seja confirmada a capacidade de pagamento da prestação.</p>
					<p>Importante: A CrediPronto! permite que cada financiamento seja formado por, no máximo, duas pessoas, as quais poderão compor rendimentos para atingir a renda mínima exigida (caso um dos clientes seja casado ou convivente marital, somente é permitida a composição de renda com seu respectivo cônjuge).</p>
				</div>	
				<div id='contentSimulator5' class='contentSimulator'>
					<p><b>1) JÁ POSSUO UM FINANCIAMENTO IMOBILIÁRIO, POSSO FAZER OUTRO?</b></p>
					<p>Sim, desde que comprove renda suficiente para assumir ambos os financiamentos, e que o novo financiamento se enquadre na política de crédito vigente.</p>
					<p><b>2) É POSSÍVEL FAZER TRANSFERÊNCIA DO FINANCIAMENTO BANCÁRIO?</b></p>
					<p>Sim. A transferência do financiamento somente será possível mediante a concessão de um novo financiamento ao solicitante, que terá como base política de crédito e legislação vigentes à época da concessão. O cálculo da renda mínima necessária está condicionado às variáveis da nova operação, tais como: valor do financiamento, idade do proponente e ao valor de avaliação do imóvel.</p>
					<p><b>3) IMÓVEIS EM CONSTRUÇÃO PODEM SER FINANCIADOS?</b></p>
					<p>Não. Apenas os imóveis que estão concluídos e com o habite-se (autorização dada pela Prefeitura para que se possa ocupar e utilizar um imóvel recém-construído ou reformado).</p>
					<p><b>4) SE A MINHA RENDA NÃO ATINGIR O MÍNIMO EXIGIDO, POSSO SOMAR A RENDA COM A DE OUTRA PESSOA?</b></p>
					<p>É permitido que cada financiamento seja formado por, no máximo, duas pessoas, que poderão compor a renda exigida. Clientes casados, inclusive por União Estável, somente é permitida a composição de renda com seu respectivo cônjuge.</p>
					<p><b>5) O QUE É ITBI E COMO ELE É CALCULADO?</b></p>
					<p>Toda compra de imóvel exige o pagamento do Imposto de Transmissão de Bens Imóveis e o registro do imóvel. Quando a aquisição é feita com um financiamento, deve-se registrar a garantia em nome do Agente Financeiro. O ITBI é cobrado pela Prefeitura local.</p>
					<p><b>6) É OBRIGATÓRIA A ABERTURA DE CONTA CORRENTE NO ITAÚ?</b></p>
					<p>Somente para o cliente comprador, pois o financiamento imobiliário da CrediPronto! é operacionalizado por meio de débito automático. Por esta razão, é necessário que possua uma conta corrente no Itaú.</p>
					<p><b>7) COMPREI UM IMÓVEL NA PLANTA, PRETENDO QUITAR NA ENTREGA DAS CHAVES, POSSO UTILIZAR O FGTS MESMO TENDO OUTRO IMÓVEL?</b></p>
					<p>Não. Se você já possui outro imóvel na localidade em que reside ou trabalha, não poderá utilizar o FGTS.</p>
					<p><b>8) MORO EM SÃO PAULO, ONDE JÁ POSSUO 2 IMÓVEIS EM MEU NOME. MAS TRABALHO HÁ MAIS DE 3 ANOS EM ITAPECERICA DA SERRA, POSSO USAR MEU FGTS PARA COMPRAR UM APARTAMENTO EM ITAPECERICA DA SERRA?</b></p>
					<p>Não, pois Itapecirica da Serra faz parte da região metropolitana de São Paulo além de ser município limítrofe de São Paulo. O proponente não pode possuir imóvel no munícipio que reside ou exerce sua ocupação principal, município limítrofe ou pertencente à região metropolitana.</p>
					<p><b>9) POSSO UTILIZAR O FGTS PARA AMORTIZAR O SALDO DEVEDOR EM UM IMÓVEL QUE FOI AVALIADO COM VALOR SUPERIOR AO PERMITIDO DE R$ 500.000,00?</b></p>
					<p>Não. O limite aceito pelas regras de uso do FGTS é que o imóvel tenha o valor de no máximo R$ 500.000,00.</p>
					<p><b>10) O QUE É MUNICÍPIO LIMÍTROFE?</b></p>
					<p>Entende-se por município limítrofe as regiões vizinhas. Exemplo: São Paulo x Guarulhos são regiões consideradas limítrofes (vizinhas), então se o cliente mora em Guarulhos e trabalha em São Paulo e quer comprar o imóvel em São Paulo poderá usar o FGTS, se não possuir imóvel. São Paulo x Praia Grande/SP não são regiões limítrofes (vizinhas), e neste caso se o cliente trabalha e mora em São Paulo e quer comprar o imóvel na Praia Grande, não poderá usar o FGTS.</p>
					<p><b>11) POSSO ADQUIRIR O IMÓVEL EM QUALQUER LUGAR, USANDO O FGTS?</b></p>
					<p>Não. Há restrições. O imóvel deve estar situado no município onde o comprador exerce sua ocupação principal, ou em município limítrofe, ou pertencente à mesma região metropolitana. Poderá ser escolhido imóvel no município onde o cliente mora, desde que comprove que não possua nenhum imóvel em todo o território nacional.</p>
					<p><b>12) POSSO USAR O FGTS PARA ADQUIRIR IMÓVEL PARA O MEU FILHO?</b></p>
					<p>Não. É vedada a sua utilização para moradia exclusiva de familiares, dependentes ou terceiros.</p>
					<p><b>13) EXISTE ENQUADRAMENTO PARA O USO DO FGTS PARA O IMÓVEL?</b></p>
					<p>Sim. O imóvel além de estar em condições para moradia, não poderá ter sido adquirido pelo(s) vendedor(es) com recursos do FGTS, nos últimos três anos.</p>
					<p><b>14) O QUE É CONTRATO DE COMPROMISSO DE COMPRA E VENDA?</b></p>
					<p>É a formalização do compromisso comercial entre o comprador e o vendedor do imóvel. Neste contrato o vendedor é chamado de promitente-vendedor e o comprador de compromissário-comprador.</p>
					<p><b>15) PODE SER FEITA VENDA DE IMÓVEL POR PROCURAÇÃO?</b></p>
					<p>Sim. Essa procuração deve ser pública.</p>
					<p><b>16) POR QUE DEVO FAZER O REGISTRO DO IMÓVEL?</b></p>
					<p>O registro do imóvel declara quem é o verdadeiro dono do imóvel. De acordo com o Código Civil Brasileiro, a propriedade (imóvel) somente é transferida após o registro da escritura no Cartório de Registro de Imóveis. Portanto, não vale assinar somente a escritura ou o contrato de venda e compra do imóvel. Deve-se realizar o registro, pois quem não registra não é considerado de fato o proprietário. O registro deve ser realizado no Cartório de Registro de Imóveis da sua localização (bairro, município ou comarca).</p>
					<p><b>17) QUAIS AS PROVIDÊNCIAS QUE DEVERÃO SER TOMADAS AO QUITAR O IMÓVEL?</b></p>
					<p>Ao quitar todas as prestações do financiamento, providencie a Escritura Definitiva no Tabelionato de Notas. Leve todos os documentos pessoais (dos compradores), respectiva prova de quitação e o contrato. Em seguida registre a Escritura no Cartório de Registro de Imóveis competente para a efetiva garantia da propriedade do imóvel. Após, peça por escrito na Prefeitura a alteração do imposto territorial para seu nome e seu endereço.</p>
					<p><b>18) POSSO UTILIZAR O FGTS PARA AMORTIZAR O FINANCIAMENTO IMOBILIÁRIO?</b></p>
					<p>Sim, desde que o financiamento se enquadre nas regras da CEF você pode utilizar para amortização extraordinária do saldo devedor para redução do valor da prestação ou amortização extraordinária do saldo devedor para redução do prazo contratual.</p>
					<p><b>19) FINANCIEI UM IMÓVEL NO VALOR DE R$ 600.000,00 E GOSTARIA DE UTILIZAR O FGTS PARA AMORTIZAR O SALDO DEVEDOR. POSSO?</b></p>
					<p>Não. Para utilização do FGTS, o limite máximo do valor do imóvel é de R$ 500.000,00.</p>
					<p><b>20) ESTOU COM PRESTAÇÕES DO MEU FINANCIAMENTO IMOBILIÁRIO EM ATRASO. GOSTARIA DE UTILIZAR O FGTS PARA FICAR EM DIA E REDUZIR PARTE DO SALDO DEVEDOR. ISSO É POSSÍVEL?</b></p>
					<p>Não. Para qualquer utilização do FGTS, tanto para abatimento nas prestações quanto para amortização ou liquidação, as prestações do financiamento deverão estar rigorosamente em dia.</p>
				</div>									
			</div>
		</div>
		<div id='rent'>
			<div class='image'>
				<span></span>
			</div>
			<div class='full'>
				<div class='title'>
					<span></span>
					Imóveis com Renda
				</div>
				<p>Imóveis são uma forma fácil e lucrativa de investimento. Quando você investe em um imóvel, pode aproveitar oportunidades que valorizam a região e elevam o valor do imóvel, a segurança de um imóvel não pode ser compara a qualquer outro ativo.</p>
				<p>Além da vantagem do patrimônio garantido pelo imóvel, você pode locá-lo para ter ainda mais rentabilidade.</p>
				<p>Além de gerar renda, um imóvel bem escolhido mantém seu valor e muitas vezes valoriza mais do que a simples correção do dinheiro investido.</p>
				<p>Em tempos de crise e altas oscilações do mercado o investimento em imóveis consegue exaltar todas as suas qualidades.</p>
				<b>Solicite as nossas análises do mercado imobiliário e seleção de imóveis com renda que serão certamente onde investirão.</b>
				<a href='contato.php' onClick='' class='btView'>
					<p>Solicitar Análises</p>
				</a>
				<hr/>
			</div>
		</div>
		<div class='clear'></div>
		</div>
		<?php include('product.php'); ?>
	</section>			
	<script>
	$(document).ready(function(){  
		$(document).scroll(function(){					
			// menu			
			if($(window).scrollTop() > ($('#rent .image').offset().top - 180)){
				$('#menuSimulator li a').removeClass('selected');
				$('#menuSim1').addClass('selected');						
			}
			else{		
				$('#menuSimulator li a').removeClass('selected');
				$('#menuSim0').addClass('selected');						
			}
			
			// box simulator
			if($(window).scrollTop() > 217){						
				if($(window).scrollTop() > ($('#rent .image').offset().top - 430)){															
					$('#simulatorLeft').removeClass('fixedSimulator');
				}
				else{									
					$('#simulatorLeft').addClass('fixedSimulator');
				}	
			}
			else{		
				$('#simulatorLeft').removeClass('fixedSimulator');
			}
		});
	});
	</script>			
<?php include('footer.php'); ?>