<?php include('header.php'); ?>	
	<header>						
		<style>
		#titleContent{
			margin:38px 0px 39px 0px;
			display:inline-block;
		}
		#boxContentProduct{
			height:889px;
		}
		#boxContentProduct #transparency{
			margin:754px 0px 0px 0px;
		}
		.boxProductLeft,
		.boxProductRight{
			width:99%;
		}
		.boxProductLeft div,
		.boxProductRight div{
			width:412px;
		}		
		.menuImoveis ul{
			display:none !important;
		}
		#contentBar{
			position:fixed;
			top:0;
			z-index:20;
		}
		</style>
		<?php
		include('bar.php');
		?>			
	</header>
	<section>
		<div class='full'>	
			<div id='titleContent'>
				<span></span>
				Nós temos o imóvel que você procura
			</div>
			<div id='boxIleft'>
				<div class='form'>
					<div id='menu1' class='menuImoveis' onClick='formSubmit(1);'>
						<h3>
							<div>
								<span></span>
							</div>
							Apartamentos
						</h3>
						<form class='form' id='formMenu1' method='get' action=''>
							<input type='hidden' name='cat' value='5'/> <!-- apartamentos -->
							<ul>
								<!--
								<li class='expanded'>
									<p onClick=''><b>x</b> Estágio</p>
									<input type='checkbox' name='' id='check1'/>
									<label for='check1'>Em Construção</label>
									<input type='checkbox' name='' id='check2'/>
									<label for='check2'>Lançamento</label>
									<input type='checkbox' name='' id='check3'/>
									<label for='check3'>Imóvel Pronto</label>
									<input type='checkbox' name='' id='check4'/>
									<label for='check4'>Lorem Ipsum</label>
								</li>						
								<li>
									<p><b>+</b> Tipo de Imóvel</p>
									<input type='checkbox' name='' id='check5'/>
									<label for='check5'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check6'/>
									<label for='check6'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check7'/>
									<label for='check7'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check8'/>
									<label for='check8'>Phasellus et risus</label>
								</li>
								<li>
									<p><b>+</b> Dormitórios</p>
									<input type='checkbox' name='' id='check9'/>
									<label for='check9'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check10'/>
									<label for='check10'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check11'/>
									<label for='check11'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check12'/>
									<label for='check12'>Phasellus et risus</label>
								</li>
								<li>
									<p><b>+</b> Cidade/Bairro</p>
									<input type='checkbox' name='' id='check13'/>
									<label for='check13'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check14'/>
									<label for='check14'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check15'/>
									<label for='check15'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check16'/>
									<label for='check16'>Phasellus et risus</label>
								</li>
								<li>
									<p><b>+</b> Valor do Imóvel</p>
									<input type='checkbox' name='' id='check17'/>
									<label for='check17'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check18'/>
									<label for='check18'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check19'/>
									<label for='check19'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check20'/>
									<label for='check20'>Phasellus et risus</label>
								</li>
								<li>
									<p><b>+</b> Características</p>
									<input type='checkbox' name='' id='check21'/>
									<label for='check21'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check22'/>
									<label for='check22'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check23'/>
									<label for='check23'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check24'/>
									<label for='check24'>Phasellus et risus</label>
								</li>
								-->
								<input type='submit' name='' value='Filtrar Imóvel'/>
							</ul>
						</form>
					</div>					
					<div id='menu2' class='menuImoveis' onClick='formSubmit(2);'>
						<h3>
							<div>
								<span></span>
							</div>
							Comerciais
						</h3>
						<form class='form' id='formMenu2' method='get' action=''>
							<input type='hidden' name='cat' value='1'/> <!-- comerciais -->
							<ul>
								<!--
								<li class='expanded'>
									<p onClick=''><b>+</b> Estágio</p>
									<input type='checkbox' name='' id='check25'/>
									<label for='check25'>Em Construção</label>
									<input type='checkbox' name='' id='check26'/>
									<label for='check26'>Lançamento</label>
									<input type='checkbox' name='' id='check27'/>
									<label for='check27'>Imóvel Pronto</label>
									<input type='checkbox' name='' id='check28'/>
									<label for='check28'>Lorem Ipsum</label>
								</li>						
								<li>
									<p><b>+</b> Tipo de Imóvel</p>
									<input type='checkbox' name='' id='check29'/>
									<label for='check29'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check30'/>
									<label for='check30'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check31'/>
									<label for='check31'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check32'/>
									<label for='check32'>Phasellus et risus</label>
								</li>
								<li>
									<p><b>+</b> Dormitórios</p>
									<input type='checkbox' name='' id='check33'/>
									<label for='check33'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check34'/>
									<label for='check34'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check35'/>
									<label for='check35'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check36'/>
									<label for='check36'>Phasellus et risus</label>
								</li>
								<li>
									<p><b>+</b> Cidade/Bairro</p>
									<input type='checkbox' name='' id='check37'/>
									<label for='check37'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check38'/>
									<label for='check38'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check39'/>
									<label for='check39'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check40'/>
									<label for='check40'>Phasellus et risus</label>
								</li>
								<li>
									<p><b>+</b> Valor do Imóvel</p>
									<input type='checkbox' name='' id='check41'/>
									<label for='check41'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check42'/>
									<label for='check42'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check43'/>
									<label for='check43'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check44'/>
									<label for='check44'>Phasellus et risus</label>
								</li>
								<li>
									<p><b>+</b> Características</p>
									<input type='checkbox' name='' id='check45'/>
									<label for='check45'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check46'/>
									<label for='check46'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check47'/>
									<label for='check47'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check48'/>
									<label for='check48'>Phasellus et risus</label>
								</li>
								-->
								<input type='submit' name='' value='Filtrar Imóvel'/>
							</ul>
						</form>
					</div>					
					<!--
					<div id='menu3' class='menuImoveis' onClick='formSubmit(3);'>
						<h3>
							<div>
								<span></span>
							</div>
							Loteamentos
						</h3>
						<form class='form' id='formMenu3' method='get' action=''>
							<input type='hidden' name='cat' value='2'/> <!-- loteamentos --
							<ul>
							<!--
								<li class='expanded'>
									<p onClick=''><b>+</b> Estágio</p>
									<input type='checkbox' name='' id='check49'/>
									<label for='check49'>Em Construção</label>
									<input type='checkbox' name='' id='check50'/>
									<label for='check50'>Lançamento</label>
									<input type='checkbox' name='' id='check51'/>
									<label for='check51'>Imóvel Pronto</label>
									<input type='checkbox' name='' id='check52'/>
									<label for='check52'>Lorem Ipsum</label>
								</li>						
								<li>
									<p><b>+</b> Tipo de Imóvel</p>
									<input type='checkbox' name='' id='check53'/>
									<label for='check53'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check54'/>
									<label for='check54'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check55'/>
									<label for='check55'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check56'/>
									<label for='check56'>Phasellus et risus</label>
								</li>
								<li>
									<p><b>+</b> Dormitórios</p>
									<input type='checkbox' name='' id='check57'/>
									<label for='check57'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check58'/>
									<label for='check58'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check59'/>
									<label for='check59'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check60'/>
									<label for='check60'>Phasellus et risus</label>
								</li>
								<li>
									<p><b>+</b> Cidade/Bairro</p>
									<input type='checkbox' name='' id='check61'/>
									<label for='check61'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check62'/>
									<label for='check62'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check63'/>
									<label for='check63'>sed vehicula lobortis</label>								
								</li>
								<li>
									<p><b>+</b> Valor do Imóvel</p>
									<input type='checkbox' name='' id='check64'/>
									<label for='check64'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check65'/>
									<label for='check65'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check66'/>
									<label for='check66'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check67'/>
									<label for='check67'>Phasellus et risus</label>
								</li>
								<li>
									<p><b>+</b> Características</p>								
									<input type='checkbox' name='' id='check68'/>
									<label for='check68'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check69'/>
									<label for='check69'>Phasellus et risus</label>
								</li>
								--
								<input type='submit' name='' value='Filtrar Imóvel'/>
							</ul>
						</form>
					</div>					
					<div id='menu4' class='menuImoveis' onClick='formSubmit(4);'>
						<h3>
							<div>
								<span></span>
							</div>
							Rurais
						</h3>
						<form class='form' id='formMenu4' method='get' action=''>
							<input type='hidden' name='cat' value='3'/> <!-- rurais --
							<ul>
								<!--
								<li class='expanded'>
									<p onClick=''><b>+</b> Estágio</p>
									<input type='checkbox' name='' id='check70'/>
									<label for='check70'>Em Construção</label>
									<input type='checkbox' name='' id='check71'/>
									<label for='check71'>Lançamento</label>
									<input type='checkbox' name='' id='check72'/>
									<label for='check72'>Imóvel Pronto</label>
									<input type='checkbox' name='' id='check73'/>
									<label for='check73'>Lorem Ipsum</label>
								</li>						
								<li>
									<p><b>+</b> Tipo de Imóvel</p>
									<input type='checkbox' name='' id='check74'/>
									<label for='check74'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check75'/>
									<label for='check75'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check76'/>
									<label for='check76'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check77'/>
									<label for='check77'>Phasellus et risus</label>
								</li>
								<li>
									<p><b>+</b> Dormitórios</p>
									<input type='checkbox' name='' id='check78'/>
									<label for='check78'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check79'/>
									<label for='check79'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check80'/>
									<label for='check80'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check81'/>
									<label for='check81'>Phasellus et risus</label>
								</li>
								<li>
									<p><b>+</b> Cidade/Bairro</p>
									<input type='checkbox' name='' id='check82'/>
									<label for='check82'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check83'/>
									<label for='check83'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check84'/>
									<label for='check84'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check85'/>
									<label for='check85'>Phasellus et risus</label>
								</li>
								<li>
									<p><b>+</b> Valor do Imóvel</p>
									<input type='checkbox' name='' id='check86'/>
									<label for='check86'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check87'/>
									<label for='check87'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check88'/>
									<label for='check88'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check89'/>
									<label for='check89'>Phasellus et risus</label>
								</li>
								<li>
									<p><b>+</b> Características</p>
									<input type='checkbox' name='' id='check90'/>
									<label for='check90'>Quisque blandit luctus</label>
									<input type='checkbox' name='' id='check91'/>
									<label for='check91'>Mauris commodo erat</label>
									<input type='checkbox' name='' id='check92'/>
									<label for='check92'>sed vehicula lobortis</label>
									<input type='checkbox' name='' id='check93'/>
									<label for='check93'>Phasellus et risus</label>
								</li>
								--
								<input type='submit' name='' value='Filtrar Imóvel'/>
							</ul>
						</form>
					</div>
					-->
				</div>
			</div>
			<div id='boxIright'>
				<div id='boxContentProduct'>
					<div id='transparency'></div>
					<?php					
					if(isset($_GET['cat'])){
						
						$menu = array(
							'5' => '1',
							'1' => '2',
							'2' => '3',
							'3' => '4'
						);
						
						echo"
							<script>
								$('#menu".$menu[$_GET['cat']]."').find('h3').addClass('select');	
								$('#menu".$menu[$_GET['cat']]."').find('ul').addClass('show');
							</script>
						";
						$sql = "select P.id, P.name, P.description, P.district, P.city, P.state, P.image from product as P INNER JOIN category as C ON  C.id  = P.id_category where P.id_category = ".$_GET['cat']; 

					
					}
					else{
						$sql = "select P.id, P.name, P.description, P.district, P.city, P.state, P.image  from product as P INNER JOIN category as C ON  C.id  = P.id_category where P.id_category = '5'";
						echo"
							<script>
								$('#menu1').find('h3').addClass('select');	
								$('#menu1').find('ul').addClass('show');
							</script>
						";
					}
					
					$x=0;
					$qry = mysql_query($sql);									
					
					// style
					$qtdRows = mysql_num_rows($qry);
					if($qtdRows < 8){
						$heightStyle = ($qtdRows * 187); // height box product					
					?>
						<style>
						#boxContentProduct{
							height: <?php echo $heightStyle; ?>px;
						}
						#boxContentProduct #transparency,
						.btView{
							display:none;
						}						
						</style>					
					<?php
					}
					else{
						$heightStyle = 1505;						
					?>
						<style>						
						#boxContentProduct{
							height: <?php echo $heightStyle; ?>px;
						}
						#boxContentProduct #transparency{
							margin-top: <?php echo $heightStyle-137; ?>px; 
						}
						</style>					
					<?php
					}					
					while($row = mysql_fetch_array($qry)){
					?>
						<li style='background-image:url(images/product/<?php echo utf8_encode($row['image']); ?>);'></li>
						<!-- products -->						
						<a href='produto.php?id=<?php echo utf8_encode($row['id']); ?>' class='boxProductLeft'>
							<img src='images/product/<?php echo utf8_encode($row['image']); ?>' alt='product'/>
							<div>
								<h2>
									<?php echo utf8_encode($row['city']); ?> / <?php echo utf8_encode($row['state']); ?>									
									<b><?php echo utf8_encode($row['district']); ?></b>
								</h2>
								<h1><?php echo utf8_encode($row['name']); ?></h1>
								<p><?php echo utf8_encode($row['description']); ?></p>								
							</div>
							<span>Saiba Mais</span>
						</a>												
					<?php					
						$x++;
					}
					
					?>
					<!-- count box -->
					<input type='hidden' id='countBox' value='<?php echo $x; ?>'/>
					<!-- /count box -->
					<!-- /products -->
				</div>
				<a href='#' onClick='return view("full");' class='btView'>
					<p>Veja todos os imóveis</p>
				</a>
			</div>
			<div class='clear'></div>
		</div>
	</section>								
<?php include('footer.php'); ?>