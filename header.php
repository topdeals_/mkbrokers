<?php include('connect.php'); ?>
<!DOCTYPE html>
<html lang='pt-br'>
<head>
	<meta charset='utf-8'/>	
	<meta name='title' content='MKBROKERS'/>
	<meta name='description' content=''/>            
	<meta name='keywords' content=''/>
	<meta property='og:title' content='MKBROKERS'/>
	<meta property='og:description' content=''/>
	<meta property='og:url' content=''/>
	<meta property='og:site_name' content='MKBROKERS'/>
	<meta property='og:type' content='website'/>
	<meta property='og:image' content='images/share.png'/> 
	<link rel='image_src' href='images/share.png'/>
	<link rel='shortcut icon' type='image/x-icon' href='images/favicon.ico'/>	
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
	<link href='css/css.css' rel='stylesheet' type='text/css' media='all'/>	
	<script src='js/jquery-1.11.0.min.js'></script>		
	<script src='js/jquery.cycle.all.js'></script>		
	<script src='js/functions.js'></script>	
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-52222116-1', 'mkbrokers.com.br');
		ga('require', 'displayfeatures');
		ga('send', 'pageview');
	</script>
	<title>MKBROKERS</title>
</head>
<body>

<!-- modal chat -->
<div id='modalChat'>	
	<div class='border'>
		<a href='#'>FECHAR</a>
		<iframe src='https://www.chatcomercial.com.br/livehelp/www/visitor/index.php?COMPANY_ID=19196&SITE_ID=22676&ssl=1&ahrefmode=true' width='500' height='350'></iframe>
	</div>
</div>
<!-- /modal chat -->