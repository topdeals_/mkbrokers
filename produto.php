<?php include('header.php'); ?>
	<style>
	#slider{
		z-index:0;
	}
	.bgSlider{
		position:absolute;
		z-index:5;
		top:0;
	}
	footer{
		width:100%;
		float:left;
	}
	#boxContentProduct{
		float:left;
	}
	.btView {		
		margin:20px 0px 20px -75px;
		position:relative;
		left:50%;
		float:left;
	}
	#titleContent{
		float:left;
	}		
	#sliderContent{
		z-index:0;
	}
	</style>
	<header>
		<?php
		// id product
		$idProduct = $_GET['id'];
		
		// product array
		$qry = mysql_query("SELECT * FROM product WHERE product.id = '".$idProduct."'");									
		$rowProduct = mysql_fetch_array($qry);			
		?>
		<!-- slider (only images product) -->
		<ul id='slider'>
			<?php
			$qry = mysql_query("SELECT photos.image FROM product LEFT JOIN photos ON product.id = photos.id_product WHERE product.id = '".$idProduct."' LIMIT 0,6");									
			while($row = mysql_fetch_array($qry)){
			?>
				<li style='background-image:url(images/product/<?php echo utf8_encode($row[0]); ?>);'></li>			
			<?php
			}
			?>			
		</ul>
		<!-- /slider (only images product) -->
		<div class='bgSlider'>					
			<div class='contentSlider'>				
				<img src='images/brand/<?php echo utf8_encode($rowProduct['brand']); ?>' alt='slider'/>
				<span>
					<h3>						
						<b><?php echo utf8_encode($rowProduct['city']); ?> | <?php echo utf8_encode($rowProduct['state']); ?></b> <?php echo utf8_encode($rowProduct['district']); ?>
					</h3>
					<h1><?php echo utf8_encode($rowProduct['name']); ?></h1>
					<h4>						
						<?php echo utf8_encode($rowProduct['description']); ?>
					</h4>						
				</span>
			</div>
		</div>
		<!-- /slider (only images product) -->
		<?php		
		include('bar.php');
		?>		
		<!-- chat -->
		<div id='chat'>
			<div></div>
			<a href='#' onClick='return showChat();'>
				<span>
					<b></b>
				</span>			
				<p>Fale agora com um Corretor Online</p>				
			</a>
		</div>
		<!-- /chat -->
	</header>
	<section>
		<?php
		// TEXT SELLER		
		if($rowProduct['text_seller']){
		?>
			<div id='seller'>
				<div class='full'>
					<b><?php echo utf8_encode($rowProduct['name_seller']); ?></b>
					<p><?php echo utf8_encode($rowProduct['text_seller']); ?></p>
				</div>
			</div>
		<?php
		}
		?>
		<div id='contentInfo'>
			<div class='full'>
				<ul>					
					<li id='info1'>
						<span></span>
						<p>Estágio da Obra <b><?php echo utf8_encode($rowProduct['stage']); ?></b></p>
					</li>
					<li id='info2'>
						<span></span>
						<p>apartamentos de<br/> <?php echo utf8_encode($rowProduct['apartment']); ?></p>
					</li>
					<li id='info3'>
						<span></span>
						<p><?php echo utf8_encode($rowProduct['area']); ?><br/> de área útil</p>
					</li>
					<li id='info5'>
						<span></span>
						<p><?php echo utf8_encode($rowProduct['garage']); ?> <br/>na garagem</p>
					</li>
					<?php if($rowProduct['dimension']){ ?>
						<li id='info4'>
							<span></span>
							<p>Terreno de<br/> <?php echo utf8_encode($rowProduct['dimension']); ?></p>
						</li>
					<?php
					}
					?>
				</ul>
				<!-- slider content -->
				<div id='sliderContentBox'>
					<a href='#' id='prevContent'></a>
					<a href='#' id='nextContent'>next</a>
					<ul id='sliderContent'>
						<?php
						$qry = mysql_query("SELECT photos.image FROM product LEFT JOIN photos ON product.id = photos.id_product WHERE product.id = '".$idProduct."'");									
						while($row = mysql_fetch_array($qry)){
						?>
							<li>
								<img src='images/product/<?php echo utf8_encode($row[0]); ?>' alt='slider'/>
							</li>			
						<?php
						}
						?>
					</ul>				
				</div>
				<!-- /slider -->				
				<div id='contentRight'>
					<div class='box'>						
						<h5><b><?php echo (utf8_encode($rowProduct['preview']))? utf8_encode($rowProduct['preview']) : 'Lançamento'; ?></b></h5>
						<hr/>
						<h6>A partir de <b><?php echo (utf8_encode($rowProduct['value']))? utf8_encode($rowProduct['value']) : 'Consulte-nos'; ?></b></h6>
					</div>
					<a href='#' onClick='return showChat();' class='chatOnline'>
						<span>
							<b></b>
						</span>			
						<p>Tire suas dúvidas com o corretor online</p>				
					</a>
					<!-- AddThis Button BEGIN -->
					<span class='addthis_toolbox addthis_default_style addthis_32x32_style'>
						<p>Compartilhe este imóvel</p>
						<a class='addthis_button_facebook'></a>
						<a class='addthis_button_orkut'></a>
						<a class='addthis_button_twitter'></a>
						<a class='addthis_button_pinterest_share'></a>
						<a class='addthis_button_google_plusone_share'></a>
						<a class='addthis_button_email'></a>
					</span>				
					<script type='text/javascript' src='//s7.addthis.com/js/300/addthis_widget.js' defer></script>
					<!-- AddThis Button END -->
				</div>
				<?php
				$qry = mysql_query("SELECT plant.image FROM product LEFT JOIN plant ON product.id = plant.id_product WHERE product.id = '".$idProduct."' LIMIT 0,4");
				if(mysql_num_rows($qry) > 0){								
				?>			
					<div id='plant'>
						<div class='full'>
							<div class='title'>
								Plantas
							</div>
							<ul>
								<?php
								$x=0;
								while($row = mysql_fetch_array($qry)){								
								?>
									<li>
										<img src='images/plant/<?php echo utf8_encode($row['image']); ?>' alt='plant'/>
										<span></span>
									</li>									
								<?php								
								}
								?>
							</ul>
							<?php $widthStyle = (mysql_num_rows($qry) * 224); ?>
							<style>
							#plant ul{
								width: <?php echo $widthStyle; ?>px;
							}
							</style>
							<p>* Plantas ilustrativas com sugestão de decoração. Os móveis, assim como alguns materiais de acabamento representados na planta, são de dimensões comercias e não fazem parte do contrato. O imóvel será entregue como indicado no memorial descritivo.</p>
							<div id='zoomContent'>
								<a href='#'>FECHAR</a>
								<span></span>
							</div>
						</div>
					</div>
				<?php
				}
				?>
			</div>
			<div class='clear'></div>
			<?php
			$qry = mysql_query("SELECT area.name FROM product LEFT JOIN area ON product.id = area.id_product WHERE product.id = '".$idProduct."'");																
			if(mysql_num_rows($qry) > 1){
				$count = round(mysql_num_rows($qry) / 2);
				$x=0;						
			?>
				<div id='plantText'>
					<div class='full'>
						<div class='title'>
							Áreas Comuns
						</div>
						<ul>
							<li>
								<?php
								while($row = mysql_fetch_array($qry)){															
									if($row['name'] != ''){
										if($x == $count){
										?>
											</li><li>
										<?php
										}
										?>	
										<p><b>•</b> <?php echo utf8_encode($row['name']); ?></p>
									<?php							
										$x++;
									}
								}
								?>	
							</li>
						</ul>
					</div>
				</div>
			<?php
			}
			else{
			?>
				<div class='full'>
					<hr class='lineHr'/>
					<style>
					.lineHr{
						margin:80px 0px 10px 0px;
					}
					</style>
				</div>
			<?php
			}			
			// MAP
			if($rowProduct['map']){
			?>				
				<div id='map'>
					<div class='full'>
						<div class='mask'>
							<a href='#'>VISUALIZAR MAPA</a>
						</div>
						<!--
						<ul>
							<b>O que tem perto?</b>
							<li id='m1'>
								<div class='select'>
									<b></b>
									<span></span>
									Academia
								</div>
							</li>
							<li id='m2'>
								<div>
									<b></b>
									<span></span>
									Bancos
								</div>															
							</li>
							<li id='m3'>
								<div>
									<b></b>
									<span></span>
									Ensino
								</div>
							</li>
							<li id='m4'>
								<div>
									<b></b>
									<span></span>
									Farmácia
								</div>
							</li>
							<li id='m5'>
								<div>
									<b></b>
									<span></span>
									Padaria
								</div>
							</li>
							<li id='m6'>
								<div>
									<b></b>
									<span></span>
									Hospital
								</div>
							</li>
							<li id='m7'>
								<div>
									<b></b>
									<span></span>
									Shopping
								</div>
							</li>
							<li id='m8'>
								<div>
									<b></b>
									<span></span>
									Supermercado
								</div>
							</li>				
						</ul>
						-->
						<div class='title'>
							Localização
						</div>				
						<p>
							<?php echo utf8_encode($rowProduct['street']); ?>,
							<?php echo utf8_encode($rowProduct['number']); ?>,
							<?php echo utf8_encode($rowProduct['district']); ?>
							<b><?php echo utf8_encode($rowProduct['city']); ?> | <?php echo utf8_encode($rowProduct['state']); ?></b>
						</p>
					</div>					
					<?php echo utf8_encode($rowProduct['map']); ?>
				</div>
			<?php
			}			
			include('product.php');
			?>
		</div>		
	</section>		
	<script>
	$(document).ready(function(){  				
		setTimeout(function(){	
			// slider
			$('#slider').before('<div id="nav">').cycle({ 
				fx: 'scrollUp', 
				delay: -6000,			
				pager: '#nav' 
			});
			
			// sliderContent
			$('#sliderContent').before('<div id="nav2">').cycle({ 
				fx: 'fade', 
				delay: -6000,			
				pager: '#nav2',
				next: '#nextContent', 
				prev: '#prevContent' 				
			});
			
			$('#prevContent, #nextContent').css({
				'display':'block'
			});
		},3500);	
				
		$(document).scroll(function(){											
			if($(window).scrollTop() > 565){
				$('#contentBar').addClass('fixed');				
				$('#chat').addClass('chatFixed');				
				$('#menu').css({
					'margin':'0px 35px 0px -9px'
				})
				$('#barRed').css({
					'background':'rgba(255,255,255,0.85)'
				});				
			}
			else{		
				$('#contentBar').removeClass('fixed');				
				$('#chat').removeClass('chatFixed');				
				$('#menu').css({
					'margin':'0px 35px 0px 0px'
				})				
				$('#barRed').css({
					'background':'rgba(158,0,57,0.85)'
				});				
			}
		});
		
		$(document).on('click', '#plant ul li img', function(){		
			$('#zoomContent span').css({
				'background-image':'url('+$(this).attr('src')+')'
			});
			$('#zoomContent').fadeIn(350);		
		});
		
		$(document).on('click', '#zoomContent a', function(){					
			$('#zoomContent').fadeOut(350);		
			return false;
		});	

		$('#_atssh').remove();
	});
	</script>
<?php include('footer.php'); ?>