<?php include('header.php'); ?>	
	<header>		
		<?php
		include('bar.php');
		?>				
	</header>
	<style>
	#contentBar{
		position:fixed;
		top:0;
		z-index:20;
	}
	</style>
	<section>
		<div id='aboutContent'>
			<div id='image'></div>
			<span>
				<b>Sobre</b> a MK Brokers
			</span>
		</div>		
		<div class='lineTitle'></div>
		<div class='texts'>			
			<p>Referência de modernidade e dinamismo no mercado, a Mk Brokers oferece aos seus clientes soluções completas em consultoria imobiliária. Com uma história consolidada, a empresa é especialista nos segmentos de Lançamentos imobiliários e imóveis prontos, prestando assessoria a incorporadores, compradores e vendedores de imóveis novos e usados. Suas atividades englobam todos os padrões, comum a marca específica para atender diversos setores do mercado imobiliário.</p>
			<p>Em parceria com a Lopes consultoria imobiliária temos uma imensa capacidade de prover o mercado com o que há de melhor em imóveis e estrutura de atendimento. Na área de financiamentos temos parceria com o Itaú, a joint venture CrediPronto oferece financiamento e consórcio imobiliário. O sucesso da parceria da Mk Brokers com a Lopes juntou a tradição e liderança no mercado da Lopes que é top of mind do setor imobiliário paulistano segundo pesquisa do IBOPE Inteligência com a exclusividade e excelência em atendimento da MK Brokers. A MK Brokers tem uma atuação muito forte no desenvolvimento do setor imobiliário na internet  e investe em informação em seu site comparada a outras empresas do setor.</p>
			<p>A MK Brokers tem o prazer de atender seus clientes com exclusividade e atenção sempre apresentando o que a de melhor no mercado imobiliário.</p>
		</div>
		<?php include('product.php'); ?>
	</section>			
<?php include('footer.php'); ?>