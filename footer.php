	<div class='clear'></div>
	<footer>
		<!-- box contact -->
		<div id='boxContact'>
			<div class='full'>
				<span id='image'></span>
				<p>
					Fale com o corretor
					<b>Entre em contato com um corretor e Conheça mais detalhes sobre os imóveis</b>
				</p>
				<ul>
					<li>
						<a href='#' onClick='return showChat();' id='contactChat' class='selected'>
							<span>
								<b></b>
							</span>
							<p>Chat Online Fale agora</p>
						</a>
					</li>
					<li>
						<a href='contato.php' id='contactMail'>
							<span>
								<b></b>
							</span>
							<p>Atendimento por email</p>
						</a>
					</li>
					<li>
						<a href='#' onClick='return false;' id='contactPhone'>
							<span>
								<b></b>
							</span>
							<p>Por telefone (11) 95230-6000 </p>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<!-- /box contact -->
		<!-- menu -->
		<div id='menuFooter'>
			<div id='barFooterWhite'></div>
			<div id='barFooterGray'></div>
			<div class='full'>
				<a href='#' id='logoFooter'>
					<img src='images/logo.png' alt='logo'/>
				</a>
				<ul>
					<li>
						<a href='sobre.php'>Sobre nós</a>
					</li>
					<li>
						<a href='imoveis.php'>Nossos Imóveis</a>
					</li>
					<li>
						<a href='financiamentos.php'>Financiamentos</a>
					</li>
					<li>
						<a href='parcerias.php'>Parcerias</a>
					</li>										
					<li>
						<a href='ouvidoria.php'>Ouvidoria</a>
					</li>
					<li>
						<a href='contato.php'>Contato</a>
					</li>
				</ul>
			</div>
		</div>
		<!-- /menu -->
		<div class='full'>
			<div id='creci'>
				creci: 73132
			</div>
		</div>
	</footer>
    <?
        include 'tags/footer.php';
    ?>
</body>
</html>