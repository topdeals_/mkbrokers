<?php include('header.php'); ?>	
	<header>				
		<style>
		#contentBar{
			position:fixed;
			top:0;
			z-index:20;
		}
		.lineTitle{			
			margin:0px auto 7px auto;
		}
		#menuSimulator{
			margin:0px;
		}
		</style>
		<?php
		include('bar.php');
		?>	
		<div id='investContent'>
			<div id='image'></div>			
		</div>	
		<div class='lineTitle'></div>		
	</header>
	<section>
		<div class='full'>				
			<div class='titlePartner'>			
				Investidores Profissionais
			</div>
			<div id='boxNotAcess'>
				<span>					
					se você não for investidor profissional, <ins>por favor NÃO ACESSE!!!</ins>
				</span>
				<a href='#' onClick='return viewContent("#notAcess");'>
					<p>Quero ser um investidor</p>
				</a>
			</div>
			<div class='texts' id='notAcess'>			
				<p>Você investidor profissional que já sabe das vantagens deste ramo investimento, sabe que existem grandes liquidações de terrenos, apartamentos e unidades em devolução. Já ouviu falar ou participa de alguma SPE vinculada a algum empreendimento, tem todo acesso as novas incorporações e loteamentos que estão em andamento, solicite a visita do nosso especialista em mercado.</p>
				<p>Teremos o prazer em conversarmos sobre as novas situações do mercado, discutir os rumos da expansão imobiliária, seus perigos e principalmente essas notícias que andam veiculando sobre a saúde do setor.</p>
				<p>Em breve faremos um newsletter especifica sobre este assunto espero que possamos contribuir com algumas matérias informações, reportagens e estudos que publicaremos.</p>
				<div class='boxInfo'>
					<span>
						Para Mais informações sobre Parcerias
					</span>
					<a href='contato.php'>
						<p>Entrar em Contato</p>
					</a>
				</div>
			</div>
			<hr class='lineHr'/>			
		</div>
		<div class='clear'></div>				
		<?php include('product.php'); ?>
	</section>								
<?php include('footer.php'); ?>