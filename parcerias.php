<?php include('header.php'); ?>	
	<header>				
		<style>
		.lineTitle{			
			margin:0px auto 7px auto;
		}
		#menuSimulator{
			margin:0px;
		}
		#contentBar{
			position:fixed;
			top:0;
			z-index:20;
		}
		</style>
		<?php
		include('bar.php');
		?>	
		<div id='partnerContent'>
			<div id='image'></div>			
		</div>	
		<div class='lineTitle'></div>
		<!--
		<div id='menuSimulator'>
			<ul>
				<li>
					<a href='#' onClick='return anchor("body");' id='menuSim0' class='selected'>Parceria</a>
				</li>
				<li>
					<a href='#' onClick='return anchor("#boxNotAcess");' id='menuSim1'>Investidores Profissionais</a>
				</li>
			</ul>
		</div>
		-->
	</header>
	<section>
		<div class='full'>	
			<div class='titlePartner'>			
				Parceria
			</div>
			<div class='texts'>			
				<p>A MK Brokers sempre trabalhou em parceria com os melhores incorporadores, construtores e players do mercado imobiliário.  Acreditamos que a cooperação entre empresas do nosso setor é muito importante, ganhamos todos, as empresas participantes e principalmente os clientes.</p>
				<p>Desenvolvemos um parceria com as empresa do Grupo Lopes para melhor atende-lo. A Pronto! Imóveis é a empresa de venda de imóveis prontos da Lopes. Ser um credenciado da rede Pronto! é atuar de forma conjunta com a Lopes, a maior e mais respeitada imobiliária do Brasil, e outras imobiliárias que são destaque nas regiões onde atuam. O resultado é um só: mais negócios para todo mundo.</p>
				<b>Confira as vantagens e soluções que só a nossa rede de parceiros tem a  oferecer.</b>
			</div>
			<div id='boxTexts'>
				<span>
					<p><b>MAIS</b> CLIENTES</p>
					Todo credenciado tem o direito de anunciar gratuitamente seus imóveis no maior portal de imóveis da internet brasileira, atraindo mais audiência.
				</span>
				<div>
					<p><b>MAIS</b> IMÓVEIS</p> 
					A MK Brokers associada a Lopes a maior comercializadora de lançamentos do país estando eles disponíveis para nossos clientes. Além disso, todos os imóveis da Rede Pronto! estarão disponíveis para venda. Muito mais variedade para oferecer aos seus clientes.
				</div>
				<span>
					<p><b>MAIS</b> GANHOS</p>  
					Com o CrediPronto!, você tem mais agilidade e rapidez na liberação de recursos e menos burocracia. As melhores taxas e mais ganhos para você.
				</span>
			</div>
			<div class='boxInfo'>
				<span>
					Para Mais informações sobre Parcerias
				</span>
				<a href='contato.php'>
					<p>Entrar em Contato</p>
				</a>
			</div>
			<hr class='lineHr'/>
			<!--
			<div class='titlePartner' id='invest'>			
				Investidores Profissionais
			</div>
			<div id='boxNotAcess'>
				<span>
					<b>ATENÇÃO</b>
					<p>se você não for investidor profissional, <ins>por favor NÃO ACESSE!!!</ins></p>
				</span>
				<a href='#' onClick='return viewContent("#notAcess");'>
					<p>Acessar</p>
				</a>
			</div>
			<div class='texts' id='notAcess'>			
				<p>Você investidor profissional que já sabe das vantagens deste ramo investimento, sabe que existem grandes liquidações de terrenos, apartamentos e unidades em devolução. Já ouviu falar ou participa de alguma SPE vinculada a algum empreendimento, tem todo acesso as novas incorporações e loteamentos que estão em andamento, solicite a visita do nosso especialista em mercado.</p>
				<p>Teremos o prazer em conversarmos sobre as novas situações do mercado, discutir os rumos da expansão imobiliária, seus perigos e principalmente essas notícias que andam veiculando sobre a saúde do setor.</p>
				<p>Em breve faremos um newsletter especifica sobre este assunto espero que possamos contribuir com algumas matérias informações, reportagens e estudos que publicaremos.</p>
				<div class='boxInfo'>
					<span>
						Para Mais informações sobre Parcerias
					</span>
					<a href='contato.php'>
						<p>Entrar em Contato</p>
					</a>
				</div>
			</div>
			-->
		</div>
		<div class='clear'></div>				
		<?php include('product.php'); ?>
	</section>								
<?php include('footer.php'); ?>