<?php include('header.php'); ?>
	<!-- loader -->
	<div id='loader'>
		<span>
			<img src='images/logo.png' alt='logo'/>
			<p>Carregando</p>
			<img src='images/loader.gif' alt='loader'/>
		</span>
	</div>
	<!-- /loader -->
	<header>
		<!-- slider -->
		<ul id='slider'>
			<?php
			$qry = mysql_query("SELECT * FROM product WHERE slider = '1' LIMIT 0,4");							
			while($row = mysql_fetch_array($qry)){
			?>				
				<li style='background-image:url(images/product/<?php echo utf8_encode($row['image']); ?>);'>
					<div class='bgSlider'>					
						<a href='produto.php?id=<?php echo utf8_encode($row['id']); ?>' class='contentSlider'>				
							<img src='images/brand/<?php echo utf8_encode($row['brand']); ?>' alt='slider'/>
							<span>
								<h3>						
									<b><?php echo utf8_encode($row['city']); ?> | <?php echo utf8_encode($row['state']); ?></b> <?php echo utf8_encode($row['district']); ?>
								</h3>
								<h1><?php echo utf8_encode($row['name']); ?></h1>
								<h4>						
									<?php echo utf8_encode($row['description']); ?>
								</h4>						
							</span>
						</a>
					</div>
				</li>			
			<?php
			}
			?>	
		</ul>
		<!-- /slider -->
		<?php		
		include('bar.php');
		?>		
		<!-- chat -->
		<div id='chat'>
			<div></div>
			<a href='#' onClick='return showChat();'>
				<span>
					<b></b>
				</span>			
				<p>Fale agora com um Corretor Online</p>				
			</a>
		</div>
		<!-- /chat -->
	</header>
	<section>
		<?php include('product.php'); ?>
	</section>		
	<script>
	$(document).ready(function(){  				
		// slider
		setTimeout(function(){	
			$('#slider').before('<div id="nav">').cycle({ 
				fx: 'scrollUp', 
				delay: -6000,			
				pager: '#nav' 
			});
		},3500);	
		
		$(document).scroll(function(){											
			if($(window).scrollTop() > 565){
				$('#contentBar').addClass('fixed');				
				$('#chat').addClass('chatFixed');				
				$('#menu').css({
					'margin':'0px 35px 0px -9px'
				})
				$('#barRed').css({
					'background':'rgba(255,255,255,0.85)'
				});				
			}
			else{		
				$('#contentBar').removeClass('fixed');				
				$('#chat').removeClass('chatFixed');				
				$('#menu').css({
					'margin':'0px 35px 0px 0px'
				})				
				$('#barRed').css({
					'background':'rgba(158,0,57,0.85)'
				});				
			}
		});
		
	});
	</script>
<?php include('footer.php'); ?>