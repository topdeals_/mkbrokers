$(document).ready(function(){  		
	images = [
		'images/about_content.jpg',
		'images/contact_content.jpg',
		'images/partner_content.jpg',
		'images/rent_content.jpg',
		'images/voice_content.jpg',
		'images/transparency.png'		
	];	
	loadImages()	
	
	dimensions();
	
	// buttons call
	$('#boxContact ul li a').hover(function(){
		$('#boxContact ul li a').removeClass('selected');
		$(this).addClass('selected');
	});/*,function(){
		$('#boxContact ul li a').removeClass('selected');
		$('#contactChat').addClass('selected');
	});*/
	
	// menu
	$('#menu').hover(function(){
		$('#barWhite').css({
			'background':'url("images/bar_white.png") no-repeat right center rgba(255,255,255,0.85)'
		});		
	},function(){
		$('#barWhite').css({
			'background':'rgba(255,255,255,0.85)'
		});		
	});
	
	// menu
	$('#map .mask a').click(function(){
		$('#map .mask').fadeOut(150);
		return false;
	});
	
	// menu imoveis	
	/*$('.menuImoveis').click(function(){
		$(this).parent('div').each(function(){
			$(this).find('ul').removeClass('show');
			$(this).find('h3').removeClass('select');			
		});
		$(this).find('ul').addClass('show');	
		$(this).find('h3').addClass('select');
	});
	
	$('.menuImoveis ul li').click(function(){
		if($(this).attr('class') != 'expanded'){
			$(this).addClass('expanded');
			$(this).find('p b').text('X');
		}		
		else{
			$(this).removeClass('expanded');
			$(this).find('p b').text('+');
		}	
	});	
	*/
	
	// loader
	setTimeout(function(){			
		$('#loader').fadeOut(300);	
	},2000);	
	
	// close modal
	$('#modalChat .border a').click(function(){		
		$('#modalChat').fadeOut(350);
		return false;
	});
});

function dimensions(){
	documentHeight = $(document).height();
	windowHeight = $(window).height();
	$('#loader').height(windowHeight);	
	marginLoader = ((windowHeight/2) - $('#loader span').height());
	$('#loader span').css({
		'margin-top' : marginLoader
	});	
	$('#zoomContent').height(documentHeight);	
	$('#zoomContent span').height(windowHeight-20);	
	
	$('#modalChat').css({
		'height':windowHeight		
	});	
	modalChat = ((windowHeight/2) - ($('#modalChat .border').height() / 2));
	$('#modalChat .border').css({		
		'margin-top':modalChat
	});	
	
};

function view(box){
	heightProduct = $('.boxProductLeft').height() + 14;		
	if(box == 'full'){
		heightBox = ($('#countBox').val());	
	}
	else{
		heightBox = ($('#countBox').val() / 2);	
	}
	$('.btView').fadeOut(350);	
	$('#transparency').fadeOut(60);
	$('#boxContentProduct').animate({
		height : (heightBox*heightProduct)+10
	}, 1000);	
	return false;
}

function viewBox(box){		
	$('.contentSimulator').slideUp(0);	
	$('#contentSimulator'+box).slideDown(200);
	$('body').animate({
		scrollTop: $('#formSimulator').offset().top - 40 
	}, 600);	
	return false;
}

function anchor(box){
	$('html, body').animate({
		scrollTop: $(box).offset().top - 122
	}, 600);	
	return false;
}

function viewContent(box){	
	$(box).slideDown(350);	
	return false;
}

function loadImages(){
	var arrayNumber = images.length;	
	for(var x=0; x<arrayNumber; x++){     	
		newImg = new Image;				
		newImg.src = images[x];
	}	
};

function valForm(){		
	error = false;
	$('#form input[type="text"], #form textarea').each(function(){		
		if($(this).val() == ''){
			$(this).css({
				'border':'#9E0039 1px solid'
			});
			error = true;
		}
		else{		
			$(this).css({
				'border':'#FFFFFF 1px solid'
			});			
		}
		
		if($(this).attr('name') == 'email'){
			filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			if(filter.exec($(this).val())){							
				$(this).css({
				'border':'#FFFFFF 1px solid'
			});
			}
			else{
				$(this).css({
					'border':'#9E0039 1px solid'
				});
				error = true;
			}			
		}
	});		
	if(error == true){
		return false;
	}
}

function showChat(){
	$('#modalChat').fadeIn(350);
	return false;
}

function formSubmit(id){
	$('#formMenu'+id).submit();	
	return false;
};